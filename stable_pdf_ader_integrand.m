function [ fx ] = stable_pdf_ader_integrand( gx, x, a, b )
% Computes integrand 

gx_to_a = gx.^a;
exp_gx_to_a = exp( - gx_to_a );

if a ~= 1
    
    z = -b * tan(a*pi/2);
    z_a = -pi/2*b * ( tan(pi*a/2)^2 + 1);
    h_a = (gx_to_a-gx) * z_a + gx_to_a .* log(gx) * z;
        
    h = (x - z) * gx + z * gx_to_a;

    fx = - (sin( h ) .* h_a ...
               + cos(h) .* gx_to_a .* log(gx) ) .* exp_gx_to_a;

else
    
    log_gx = log(gx);
    h_a = b/pi * gx .* log_gx.^2;
   
    h = x*gx + 2/pi*b* gx .* log_gx;

    fx = - (sin( h ) .* h_a ...
                + cos(h) .* gx_to_a .* log(gx) ) .* exp_gx_to_a;

end

end

