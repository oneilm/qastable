function [ fx ] = stable_pdf_xder_integrand( gx, x, a, b )
% Computes integrand 

gx_to_a = gx.^a;
exp_gx_to_a = exp( - gx_to_a );

if a ~= 1
    
    z = -b * tan(a*pi/2);
        
    h = (x - z) * gx + z * gx_to_a;
    fx = - (sin( h ) .* gx ) .* exp_gx_to_a;

else
    
    log_gx = log(gx);
    h = x*gx + 2/pi*b* gx .* log_gx;
    fx = - (sin( h ) .* gx) .* exp_gx_to_a;

end

end

