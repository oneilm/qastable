function [ f ] = stable_cdf_integrand( t, x, a, b )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

gx_to_a = t.^a;
exp_gx_to_a = exp( - gx_to_a );

z = -b * tan(a*pi/2);

h = (x - z) * t + z * gx_to_a;
f = sin( h ) .* exp_gx_to_a ./ t;



end

