function [ val ] = stable_cdf_series_infinity( x, a, b, max_coef )
%fastest of the bunch
zeta = -b * tan((pi*a)/2);

k_index = linspace(0, max_coef, max_coef+1);

sign = -1;
  
gamma_part = gamma( a * (k_index+1)) ./ ...
             (factorial(k_index+1));

sqrt_1_plus_zeta = sqrt(1+zeta^2);
geometric_part = 1;

sin_part = sin( (pi/2 * a - atan(zeta)) * (k_index+1));

x_to_minus_a = (x-zeta).^(-a);
x_part = 1;
 
val = zeros(size(x));

for k = 0:max_coef
    
   sign = -sign;
   
   geometric_part = geometric_part * sqrt_1_plus_zeta;
   x_part = x_part .* x_to_minus_a;
   
   term = sign...
         .* gamma_part(k+1) ...
         .* geometric_part ...
         .* sin_part(k+1)...
         .* x_part;
     
   val = val + term;
   
end

val = 1 - val/pi;

end

