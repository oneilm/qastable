function [ val ] = stable_pdf_fourier_integrand( t, x, a, b )

if  a == 1       
    h = (x) .* t + (2 / pi) * b * t .* log( t );
else
    z = -b * tan((pi*a)/2);
    h = (x - z) * t + z * t.^a;
end

val = cos( h ) .* exp( - (t.^a) );

end

