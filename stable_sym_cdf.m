function [ cdf ] = stable_sym_cdf( x, a )%, mu, sigma)
% computes symmetric stable density values

%x = (x - mu) / sigma;
if a == 1
    
    cdf = atan(x)/pi + 1/2;  % Cauchy CDF
    
elseif .5 <= a && a < 2 
   
    % number of terms to be used in the series expansion
    n_inf = 42;

    % minimum x for which the series at infinity, truncated to n terms,
    % is accurate to machine precision.
    % using the same bound as for pdf-series here. This is justified
    % because the cdf-series bound is strictly less then this.
    min_inf_x = (a /(pi * eps) ...
            .* gamma(a*n_inf)./gamma(n_inf) ).^(1./(a*n_inf-1));
    
    cdf = zeros(size(x));
    
    neg_inf_cond = x < -min_inf_x;
    if any(neg_inf_cond)
        x( neg_inf_cond ) = - x( neg_inf_cond );
    end
    
    inf_cond = min_inf_x < x;
    if any( inf_cond )
        cdf( inf_cond ) = stable_cdf_series_infinity(...
            x(inf_cond), a, 0, n_inf);
    end
   if any(isinf(cdf))
        pause
    end
    fourier_cond = ~inf_cond;
    if any( fourier_cond )
        cdf( fourier_cond ) = stable_sym_cdf_integral( ...
            x( fourier_cond ) , a);
    end
 
else
    
    display('Error: Input a has to be between .5 and 2');
    cdf = Nan(size(x));

end

end

