function [ fx ] = stable_pdf_bder_integrand( gx, x, a, b )
% Computes integrand 

gx_to_a = gx.^a;
exp_gx_to_a = exp( - gx_to_a );

if a ~= 1
    
    z = -b * tan(a*pi/2);
    z_b = -tan( pi*a/2);
    h_b = (gx_to_a - gx) .* z_b;

    h = (x - z) * gx + z * gx_to_a;
    
    fx = - sin( h ) .* h_b .* exp_gx_to_a;


else
    
    log_gx = log(gx);
    h_b = 2/pi * gx .* log_gx;
        
    h = x*gx + 2/pi*b* gx.*log_gx;

    fx = - sin( h ) .* h_b .* exp_gx_to_a;

end

end

