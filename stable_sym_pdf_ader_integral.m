function [ ader ] = stable_sym_pdf_ader_integral( x, a)
% computes the derivative w.r.t. alpha of symmetric stable densities
% between .5 <= alpha <= 2
% assumes n_inf = 43, that is |x| < B_inf(43)

if .5 <= a
    
    num_quad = 43; 
    
    gx(1) = 2.1848525062602291e-07; 
    gx(2) = 5.5686422934044713e-06; 
    gx(3) = 4.9206998317511994e-05; 
    gx(4) = 2.4487427581113339e-04; 
    gx(5) = 8.4228609170440083e-04; 
    gx(6) = 2.2438074173485216e-03; 
    gx(7) = 4.9660615211205858e-03; 
    gx(8) = 9.5570336029522415e-03; 
    gx(9) = 1.6493125464770400e-02; 
    gx(10) = 2.6090802039996908e-02; 
    gx(11) = 3.8465764915946450e-02; 
    gx(12) = 5.3548899900510226e-02; 
    gx(13) = 7.1139367918425608e-02; 
    gx(14) = 9.0965561665450695e-02; 
    gx(15) = 1.1273453218299893e-01; 
    gx(16) = 1.3616420097331450e-01; 
    gx(17) = 1.6100060318997464e-01; 
    gx(18) = 1.8702464791819093e-01; 
    gx(19) = 2.1405246794530811e-01; 
    gx(20) = 2.4193236003978172e-01; 
    gx(21) = 2.7054030631373427e-01; 
    gx(22) = 2.9977527096799955e-01; 
    gx(23) = 3.2955489343338340e-01; 
    gx(24) = 3.5981182609757312e-01; 
    gx(25) = 3.9049075194797517e-01; 
    gx(26) = 4.2154601558012372e-01; 
    gx(27) = 4.5293976609534503e-01; 
    gx(28) = 4.8464050937996112e-01; 
    gx(29) = 5.1662198287301908e-01; 
    gx(30) = 5.4886228077043842e-01; 
    gx(31) = 5.8134316976109790e-01; 
    gx(32) = 6.1404954323020589e-01; 
    gx(33) = 6.4696894788512282e-01; 
    gx(34) = 6.8009112156473672e-01; 
    gx(35) = 7.1340750373734474e-01; 
    gx(36) = 7.4691077896692937e-01; 
    gx(37) = 7.8059424543346689e-01; 
    gx(38) = 8.1445158874244039e-01; 
    gx(39) = 8.4847499894211031e-01; 
    gx(40) = 8.8264947508964775e-01; 
    gx(41) = 9.1693032962197996e-01; 
    gx(42) = 9.5113624634661176e-01; 
    gx(43) = 9.8406977355750436e-01; 

    gw(1) = 9.0658971250103289e-07; 
    gw(2) = 1.4374457622687654e-05; 
    gw(3) = 9.0812229398240038e-05; 
    gw(4) = 3.4182921305480636e-04; 
    gw(5) = 9.2072732267497949e-04; 
    gw(6) = 1.9698579812822822e-03; 
    gw(7) = 3.5677836302077459e-03; 
    gw(8) = 5.6951236167853249e-03; 
    gw(9) = 8.2305095610982169e-03; 
    gw(10) = 1.0983804312071946e-02; 
    gw(11) = 1.3753638812519045e-02; 
    gw(12) = 1.6378095206927748e-02; 
    gw(13) = 1.8756738749236126e-02; 
    gw(14) = 2.0846294780145817e-02; 
    gw(15) = 2.2644357389348085e-02; 
    gw(16) = 2.4172549904752663e-02; 
    gw(17) = 2.5463740634052271e-02; 
    gw(18) = 2.6553759930776619e-02; 
    gw(19) = 2.7476673649169333e-02; 
    gw(20) = 2.8262520677007491e-02; 
    gw(21) = 2.8936604878689209e-02; 
    gw(22) = 2.9519658746955595e-02; 
    gw(23) = 3.0028411103219636e-02; 
    gw(24) = 3.0476273952893877e-02; 
    gw(25) = 3.0873999214700482e-02; 
    gw(26) = 3.1230243487995874e-02; 
    gw(27) = 3.1552027112942561e-02; 
    gw(28) = 3.1845096786920837e-02; 
    gw(29) = 3.2114210550087556e-02; 
    gw(30) = 3.2363355244254370e-02; 
    gw(31) = 3.2595912799751957e-02; 
    gw(32) = 3.2814765941993410e-02; 
    gw(33) = 3.3022341937367763e-02; 
    gw(34) = 3.3220584551924552e-02; 
    gw(35) = 3.3410974441995811e-02; 
    gw(36) = 3.3594457421704617e-02; 
    gw(37) = 3.3771450203687485e-02; 
    gw(38) = 3.3942002257281313e-02; 
    gw(39) = 3.4102666403589152e-02; 
    gw(40) = 3.4239640682277320e-02; 
    gw(41) = 3.4300480229194880e-02; 
    gw(42) = 3.3985705599652069e-02; 
    gw(43) = 3.0468375369044814e-02; 

end

ader = zeros(size(x));

rank_scaling = ( - log(eps) )^(1/a);
gx = rank_scaling * gx;
gw = rank_scaling/pi * gw;

gx_to_a = gx.^a;
exp_gx_to_a = exp( - gx_to_a );

b = 0; % symmetric case

if a ~= 1
    
    z = -b * tan(a*pi/2);
    z_a = -pi/2*b * ( tan(pi*a/2)^2 + 1);
    h_a = (gx_to_a-gx) * z_a + gx_to_a .* log(gx) * z;

    for j = 1:num_quad
        
        h = (x - z) * gx(j) + z * gx_to_a(j);
        
        ader = ader - gw(j) * (sin( h ) .* h_a(j)...
                   + cos(h) .* gx_to_a(j) * log(gx(j)) ) .* exp_gx_to_a(j);
        
    end
   
else
    
    log_gx = log(gx);
    h_a = b/pi * gx .* log_gx.^2;
    
    for j = 1:num_quad
        
        h = x*gx(j) + 2/pi*b*gx(j)*log_gx(j);
        
        ader = ader - gw(j) * (sin( h ) .* h_a(j) ...
                   + cos(h) .* gx_to_a(j) * log(gx(j)) ) .* exp_gx_to_a(j);
        
    end
end

end

