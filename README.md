qastable provides functions for the numerical computation of stable densities,
distribution functions, and partial derivatives of the densities.
The code relies on optimized generlized Gaussian quadrature rules, and 
asymptotic expansions.
This makes it possible to compute stable distributions very efficiently 
up to virtually machine precision.
Indeed, qastable outperforms most adaptive integrations schemes for 
stable distributions by several orders of magnitude.
The file stable_demo.m shows how to use the core functions of qastable.